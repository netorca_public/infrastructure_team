# Demo Infrastructure Team Repository

This repository represents how a ServiceOwner team would manage the Services they would like to publish on the Service Catalogue and make available to Consumers. 

Services are maintained within this repository as JSONSchema objects and are versioned to allow for updating and modifications.

## Requirements

This repository requires the following environment variables to be set for the CI/CD process:
* netorca_key -> an API key that can be generated for the team on the NetOrca GUI

In addition the .netorca/config.json requires some team specific configuration which is available from the GUI. 

## Schemas

Schemas are maintained as .json objects within the .netorca folder.
Any updates to the schema will require a change to the 'version' field in the json, this is to ensure that schema modifications are controlled and only published when required. 

## Change process

The typical change process for modification,  deletion of creation of ServiceItems with this repo would be as follows: 

1. Branch from 'main' created, Service Schema modifications are mode. 
2. Push to branch and merge request created
3. CI/CD process will lint the schemas and send to NetOrca for validation, if valid build process will pass
4. Merge request can be reviewed and approved as normal by the team.
5. Changes are sent to NetOrca, new Service created or new Schema version is applied.


## Using this repo in the demo

Anyone is able to create a new branch/merge request to this repository and test the NetOrca validation. 

Please contact someone from the NetOrca team if you would like you merge request approved and would like to see the change instances that are created. 

